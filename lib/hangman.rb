class Hangman
  attr_reader :guesser, :referee, :board, :players

  def initialize(players = {})
    defaults = {
      guesser: "John",
      referee: "Bob"
    }
    @players = defaults.merge(players)
    @guesser = @players[:guesser]
    @referee = @players[:referee]
  end

  def setup
    word_length = @referee.pick_secret_word
    @guesser.register_secret_length(word_length)
    @board = "_" * word_length
  end

  def take_turn
    player_guess = @guesser.guess(@board)
    referee_guess_check = @referee.check_guess(player_guess)
    update_board(player_guess, referee_guess_check)
    @guesser.handle_response(player_guess, referee_guess_check)
  end

  def update_board(lttr, arr_lttr_check)
    if arr_lttr_check.empty?
      @board = @board
    else
      arr_lttr_check.each { |idx| @board[idx] = lttr }
    end
  end


end

class HumanPlayer
end

class ComputerPlayer
  attr_reader :dictionary, :secret_word, :name, :candidate_words

  def initialize(dictionary, name = "ComputerPlayer")
    @dictionary = dictionary
    @secret_word = dictionary.shuffle[0]
    @name = name
    @candidate_words = dictionary
  end

  def pick_secret_word
    @secret_word.length
  end

  def check_guess(lttr)
    letters = @secret_word.chars
    idx_pos = []
    letters.each_with_index do |letter, idx|
      if letter == lttr
        idx_pos << idx
      else
        next
      end
    end
    idx_pos
  end

  def register_secret_length(word_length)
    @candidate_words = @candidate_words.select {|word| word.length == word_length}
  end

  def guess(board)

    letters = ("a".."z").to_a.reject {|letter| board.include?(letter)}
    candidate_letters = @candidate_words.join
    letters.reduce do |acc, el|
      if candidate_letters.count(el) > candidate_letters.count(acc)
        el
      else
        acc
      end
    end
  end

  def handle_response(lttr, arr)
    wrong_words = []
    @candidate_words.each do |word|
      if arr.empty? && word.include?(lttr)
        wrong_words << word
      elsif word.count(lttr) != arr.length
        wrong_words << word
      end
      arr.each do |idx|
        if word[idx] != lttr
          wrong_words << word
        end
      end
    end
    @candidate_words = @candidate_words.reject {|word| wrong_words.include?(word)}
  end
end
